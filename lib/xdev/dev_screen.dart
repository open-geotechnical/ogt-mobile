import 'package:flutter/material.dart';

import '../drawer.dart';
import '../G.dart' as G;

//import "ags4_group_screen.dart";

class DevScreen extends StatefulWidget {
  @override
  DevScreenState createState() => DevScreenState();
}

class DevScreenState extends State<DevScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: AppDrawer(),
        appBar: AppBar(
          title: Text("Develop and Test"),
          backgroundColor: Colors.yellow,
        ),
        body: Column(
          children: <Widget>[
            Text(
                "All below is for testing and preview, have fun, to make up the app"),
            RaisedButton(
                child: const Text("Click for hello test"),
                onPressed: () => Navigator.pushNamed(context, '/project')
            ),
            RaisedButton(
                child: const Text("Project Wizard"),
                onPressed: () => Navigator.pushNamed(context, '/project')
            ),
            RaisedButton(
                child: const Text("PROJ Form"),
                onPressed: () => Navigator.pushNamed(context, '/project')
            ),
            RaisedButton(
                child: const Text("Clock + Stopwatch"),
                onPressed: () => Navigator.pushNamed(context, '/clock')
            )
          ],
        ));
  }
}
