
import 'package:flutter/material.dart';

import 'home_screen.dart';

import "ags4/ags4_main_screen.dart";

import "gps/gps_page.dart";
import "map/google_map.dart";

import "projects/projects_screen.dart";
import "projects/project_wizz_screen.dart";
import "projects/project_screen.dart";

import "xdev/dev_screen.dart";
import "date_time/date_time_widgets.dart";

import 'G.dart' as G;


// main()
//Future<void> main() async {
void main() {
  //cameras = await availableCameras();
  G.init();
  runApp( OGTApp() );
}



/// Default theme - for extend hopefully...
//
// the mad future idea is that this can be set
// by corps and skinning for clients + staff etc
var ogtTheme = ThemeData(
  primarySwatch: Colors.orange,
);


class OGTApp extends StatelessWidget {
  OGTApp();

  factory OGTApp.forDesignTime() {
    // TODO: add arguments
    return new OGTApp();
  }


  @override
  Widget build(BuildContext context) {


    return MaterialApp(
      title: 'Open GeoTechnical',
      theme: ogtTheme,

      initialRoute: G.devMode ? '/ags4' : "/",

      routes: {
          '/':          (context) => HomeScreen(),

          '/projects':  (context) => ProjectsScreen(),
          '/project_wizz':  (context) => ProjectWizardScreen(),
          '/project':   (context) => ProjectScreen(),

          '/ags4':      (context) => Ags4DataDictScreen(),

          '/gps':       (context) => GpsPage(),
          '/map':       (context) => MapPage(),
          '/clock':     (context) => ClockScreen(),
          '/xdev':      (context) => DevScreen(),
      },

      debugShowCheckedModeBanner: false,
      //debugShowMaterialGrid: true,

    );
  }
}
