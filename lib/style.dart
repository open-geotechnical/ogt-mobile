
import 'package:flutter/material.dart';

const Color main = Colors.orange;
const Color project = Colors.blue;
const Color ags4 = Colors.green;

const Color groupReq = Colors.deepPurple;
const Color groupNotReq = Colors.green;

const Color actionBg = Colors.green;
const Color actionFg = Colors.limeAccent;
