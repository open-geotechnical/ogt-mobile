
import 'package:flutter/material.dart';

//import "../ags4/ags4_models.dart" as ags4;
import "data_models.dart";

String _v_notEmpty(String v){
  if(v.trim().isEmpty){
    return "Required";
  }

}

FormField createFormField(Heading hd, TextEditingController controller){

    var decor = InputDecoration(
      labelText: hd.head_description,
      helperText: hd.head_code
    );

    var widget = null;
    var keyType = null;
    var validator = null;

    if(hd.data_type == "X") {
      keyType = TextInputType.text;
      if(hd.required == true){
        validator = _v_notEmpty;
        print("VVVV##############VVVvv=== " + hd.head_code);
      }

    } else if(hd.data_type.endsWith("DP")) {
      keyType = TextInputType.number;
    }


    widget = TextFormField(
      controller: controller,
      keyboardType: keyType,
      decoration: decor,
      validator: validator,
    );
    return widget;
}

//==========================================================================
class FormWidget extends StatefulWidget {

  Group group;
  int dataRowIndex;

  FormWidget({Key key,
    @required Group this.group,
    @required int this.dataRowIndex
  }): super(key: key);

  @override
  FormWidgetState createState() => FormWidgetState();

}

class FormWidgetState extends State<FormWidget>
{

  final _formKey = GlobalKey<FormState>();

  //Group get group => this.widget.project.getGroup(this.widget.group_code);
  List<FormField> widgets = new List<FormField>();
  List<TextEditingController> controllers = new List<TextEditingController>();

  @override
  Widget build(BuildContext context) {
    return _buildForm();

//    return Column(
//        children: <Widget>[
//          _buildHeader(),
//          Expanded(child:_buildForm())
//        ],
//
//    );
  }

//  Widget _buildHeader(){
//
//    return Row(
//      mainAxisAlignment: MainAxisAlignment.start,
//      children: <Widget>[
//        Expanded(child:Text(this.widget.group.group_description)),
//        RaisedButton.icon(
//          color: Colors.green,
//          icon: Icon(Icons.save), label: Text("Save"),
//          onPressed: this.save
//        )
//      ],
//    );
//
//  }

  Widget _buildForm() {

    var rec = this.widget.group.data_rows[this.widget.dataRowIndex];

    for (var i = 0; i < this.widget.group.headings.length; i++) {
      var hd = this.widget.group.headings[i];

      var control = new TextEditingController();
      this.controllers.add(control);
      control.text = rec[hd.head_code];
      control.addListener(() {
          setState(() {
            print(hd.head_code + "=" + control.text);
            this.widget.group.data_rows[this.widget.dataRowIndex][hd.head_code] = control.text;
          });
      });

      var widget = createFormField(hd, control);
      this.widgets.add(widget);

    }

    return Form(
      key: _formKey,
      child: ListView(
        padding: EdgeInsets.all(5.0),
        children: this.widgets,
      )
    );
  }

  @override
  void initState(){
    super.initState();
  }

  void save(){
    print("save");
    var v = this._formKey.currentState.validate();
    print(v);

    var rec = Map<String, String>();
    for (var i = 0; i < this.widget.group.headings.length; i++) {
      var hd = this.widget.group.headings[i];
      var v = this.controllers[i].text;
      rec[hd.head_code] = v;
      //print(hd.head_code + "=" + v);
    }
    //Navigator.pop(context, rec);

  }

}

//==========================================================================
class GroupsListView extends StatelessWidget {

  Project project;

  GroupsListView(Project this.project);

  @override
  Widget build(BuildContext context){

    var grpCodes = project.groups_sorted_codes();

    return ListView.builder(
      itemCount: grpCodes.length,
      padding: const EdgeInsets.all(15.0),
      itemBuilder: (context, index) {

        Group grp = this.project.groups_map[grpCodes[index]];

        return ListTile(
          title: Text('${grp.group_description}',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500)
          ),
          subtitle: Text('${grp.group_code}',
            style: TextStyle(color: grp.group_required == true ? Colors.purple : Colors.green)
          ),
          trailing: CircleAvatar(
            backgroundColor: grp.group_required == true ? Colors.grey : Colors.green ,
            child: Text('${grp.data_rows.length}',
              style: TextStyle(color: Colors.white),
            ),
          ),
          onTap: (){
            //this.edit_group(index, grp.group_code);
            print("on_edit: " + grp.group_code );
            //widget.onSelected(storesFiltered[index]);
          },
        );
      }
    );
  }
}
