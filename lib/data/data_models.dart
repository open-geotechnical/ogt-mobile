
import 'dart:convert';


import "../ags4/ags4_models.dart" as ags4;
import "../G.dart" as G;

const String PROJECT_FREFIX = "project-";

Future<List<Map<String, dynamic>>> listProjects() async {

  // get files
  List<String> files = await G.localStorage.listDir(PROJECT_FREFIX);
  print(files);

  List<Map<String, dynamic>> recs = new List<Map<String, dynamic>>();
  files.forEach((String fname){

      //var contents = G.localStorage.readFile(fname);
      G.localStorage.readFile(fname)
        .then((String contents) {


          print(fname);
          print("--contents--");
          print(contents);
          var data = G.fromJSON(contents);
          print("--data--");
          print(data);
          //var rec = new Map<String, String>();
          var rec = data['PROJ'][0];
          print(rec);
          rec['file_name'] = fname;

          recs.add(rec);
      });

  });
  return recs;

}

class Project {

  String fileName = null;
  List<String> groups_list = new List<String>();
  Map<String, Group> groups_map = new Map<String, Group>();

  String toString(){
    var s = "<Project ";
    s += groups_list.join(",");
    s += ">";
    print(this.groups_map);
    return s;
  }

  void addRequiredGroups(){

    var grps_req = G.ags4DD.groups_required;
    print(grps_req);
    for(var i = 0; i < grps_req.length; i++){
      this.addGroupFromAgs4(grps_req[i]);
      //var g = Group.createFromAgs4(this, grps_req[i]);
      //groups_list.add(g.group_code);
      //this.groups_map[g.group_code] = g;
    }
  }

  Project.createDefault(){

    this.addRequiredGroups();
  }

  Project.createFromTemplate(ProjectTemplate tpl){

    this.addRequiredGroups();
    tpl.xgroups.forEach((grp_code){
      this.addGroupFromCode(grp_code);
    });
  }

  String addGroupFromCode(String grp_code){

    if(this.groups_list.contains(grp_code)){
      return "Error: already in group";
    }

    var agsg = G.ags4DD.getGroup(grp_code);
    if(agsg != null){
      addGroupFromAgs4(agsg);
      return "";
    }

    return "Error: not foiund ?";
  }
  Group addGroupFromAgs4(ags4.Group ags4groupDef){
    var g = Group.createFromAgs4(this, ags4groupDef);
    this.groups_list.add(g.group_code);
    this.groups_map[g.group_code] = g;
    return g;
  }

  void removeGroup(String grp_code){
    this.groups_list.remove(grp_code);
    this.groups_map.remove(grp_code);
  }

  Group getGroup(String grp_code){
    return this.groups_map[grp_code];
  }

  List<String> groups_sorted_codes(){
    return G.ags4DD.sortGroups(this.groups_list);
  }

  List<Group> groups_sorted(){
    return groups_sorted_codes().map((String gc) => this.groups_map[gc]).toList();
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> data = new Map<String, dynamic>();
    print("---------");
    print(this.groups_sorted());
    groups_sorted().forEach((Group grp) {
      print(grp.group_code);
      data[grp.group_code] = grp.data_rows;
    });
    return data;
  }

  String toJSON(){
    return G.toJSON(this.toMap());
  }


  Error save(){
    if(true) { //this.fileName == null){
      var s = new DateTime.now().millisecondsSinceEpoch.toString();
      this.fileName = PROJECT_FREFIX +  s;
    }

    G.localStorage.writeFile(this.fileName, this.toJSON());
    print(this.fileName);
  }

}

//============================================================
class Group {

  Project _project;
  ags4.Group _dd;
  List<Heading> headings;

  String get group_code => this._dd.group_code;
  String get group_description => this._dd.group_description;
  bool get group_required => this._dd.group_required;

  List<Map<String, String>> data_rows = new List<Map<String, String>>();

  Group.createFromAgs4(Project parentProject, ags4.Group agsGrp){

    this._project = parentProject;
    this._dd = agsGrp;

    this.headings = new List<Heading>();
    agsGrp.headings.forEach((agsdHd) {
        var newHd = Heading.createFromAgs4(agsdHd);
        this.headings.add(newHd);
    });
    
  }

  int createDataRow(){
    Map<String, String> rec = new Map<String, String>();
    this.headings.forEach((Heading hd){
      rec[hd.head_code] = "";
      //rec[hd.head_code] = hd.head_code + " - data";
    });
    this.data_rows.add(rec);
    return this.data_rows.length - 1;
  }

  Map<String,String> toMap(){

    Map<String,String> map = Map<String,String>();

    for(var i = 0; i < this.data_rows.length; i++) {
      var rec =
      this.headings.forEach((Heading head) {

      });
    }

    return map;

  }

}

//============================================================
class Heading {

  ags4.Heading _dd;

  Heading.createFromAgs4(ags4.Heading ags4Heading){

    this._dd = ags4Heading;

  }

  String get head_code => this._dd.head_code;
  String get head_description => this._dd.head_description;
  String get data_type => this._dd.data_type;
  bool get required => this._dd.required;
}




//============================================================
class ProjectTemplate {

  String title;
  List<String> xgroups;

  ProjectTemplate(String this.title, List<String> this.xgroups);

}

List<ProjectTemplate> projectTemplates(){

  List<ProjectTemplate> lst = new List<ProjectTemplate>();
  lst.add( ProjectTemplate("Base", []) );
  lst.add( ProjectTemplate("Drilling", ["SAMP", "LOCA", "CHOC"]) );
  lst.add( ProjectTemplate("Insitu CBR", ["SAMP", "LOCA", "ICBR", "CHOC"]) );
  lst.add( ProjectTemplate("Insitu Penetrometer", ["SAMP", "LOCA", "IPEN"]) );

  return lst;
}



