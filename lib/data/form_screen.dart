
import 'package:flutter/material.dart';


import '../G.dart' as G;

import "data_models.dart";
import "data_widgets.dart";


class FormScreen extends StatefulWidget {

  Project project;
  String group_code;

  FormScreen({Key key, Project this.project, String this.group_code}): super(key: key);

  @override
  FormScreenState createState() => FormScreenState();

}

class FormScreenState extends State<FormScreen>
{

  final _formKey = GlobalKey<FormState>();

  Group get group => this.widget.project.getGroup(this.widget.group_code);
  List<FormField> widgets = new List<FormField>();
  List<TextEditingController> controllers = new List<TextEditingController>();

  @override
  Widget build(BuildContext context) {


    return  Scaffold(


      appBar: AppBar(
        title: Text(this.group.group_code),
        backgroundColor: Colors.yellow,
        //bottom: PreferredSize(child: Text(this.group.group_description)) ,
      ),

      body: Column(
        children: <Widget>[
          _buildHeader(),
          Expanded(child:_buildForm())
        ],
      )


    );
  }

  Widget _buildHeader(){

    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Expanded(child:Text(this.group.group_description)),
        RaisedButton.icon(
          color: Colors.green,
          icon: Icon(Icons.save), label: Text("Save"),
            onPressed: this.save
        )
      ],
    );

  }

  Widget _buildForm() {

    for (var i = 0; i < this.group.headings.length; i++) {
      var hd = this.group.headings[i];

      var control = new TextEditingController();
      this.controllers.add(control);

      var widget = createFormField(hd, control);
      this.widgets.add(widget);

    }

    return Form(
      key: _formKey,
      child: ListView(
        padding: EdgeInsets.all(5.0),
        children: this.widgets,
      )
    );
  }

  @override
  void initState(){
    super.initState();
  }

  void save(){
    print("save");
    var rec = Map<String, String>();
    for (var i = 0; i < this.group.headings.length; i++) {
      var hd = this.group.headings[i];
      var v = this.controllers[i].text;
      rec[hd.head_code] = v;
      //print(hd.head_code + "=" + v);
    }
    Navigator.pop(context, rec);

  }


}
