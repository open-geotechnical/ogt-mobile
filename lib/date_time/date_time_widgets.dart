import 'dart:async';

import 'package:flutter/material.dart';
import '../drawer.dart' as drawer;

//==================================================

class Dependencies {
  final List<ValueChanged<ElapsedTime>> timerListeners =
      <ValueChanged<ElapsedTime>>[];
  final TextStyle textStyle = const TextStyle(fontSize: 50.0);
  final Stopwatch stopwatch = new Stopwatch();
  final int timerMillisecondsRefreshRate = 500;
}

class ElapsedTime {
  final int hundreds;
  final int seconds;
  final int minutes;

  ElapsedTime({
    this.hundreds,
    this.seconds,
    this.minutes,
  });
}

//==================================================
class ClockScreen extends StatefulWidget {
  ClockScreen({Key key}) : super(key: key);

  factory ClockScreen.forDesignTime() {
    // TODO: add arguments
    return new ClockScreen();
  }

  @override
  ClockScreenState createState() => ClockScreenState();
}

class ClockScreenState extends State<ClockScreen> {
  final Dependencies dependencies = new Dependencies();
  //bool _running = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //= The app bar has the start buttons
      appBar: AppBar(
        title: Text("Clock & Stopwatch"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'This stopwatch widget is targeted and CBR and alike. In future ideas are an auto voice countdown, sensor integration, video etc',
                //..style: Theme.of(context).textTheme.display1,
              ),
            ),
            Container(
              height: 2,
              color: dependencies.stopwatch.isRunning ? Colors.green : Colors.pink,
            ),
            Container(
              //color: dependencies.stopwatch.isRunning ? Colors.green : Colors.pink,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(dependencies.stopwatch.isRunning ? "Running" : "Stopped",
                    style: TextStyle(color: Colors.black)),
              ),
            ),
            new TimerText(dependencies: dependencies),
            Container(
              height: 2,
              color: dependencies.stopwatch.isRunning ? Colors.green : Colors.pink,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton.icon(
                  color: Colors.green,
                  icon: Icon(Icons.play_arrow),
                  label: Text("Start"),
                  onPressed: dependencies.stopwatch.isRunning ? null : this.timerStart,
                ),
                Container(
                  width: 10,
                ),
                RaisedButton.icon(
                  color: Colors.pink,
                  icon: Icon(Icons.stop),
                  label: Text("Stop"),
                  onPressed: dependencies.stopwatch.isRunning ? this.timerStop : null,
                ),
              ],
            ),
            new Expanded(child: Container()),
          ],
        ),
      ),

      drawer: drawer.AppDrawer(),
    );
  }

  void timerStart() {
    if (dependencies.stopwatch.isRunning) {
      //stopwatch.stop();
      // maybe restart
    } else {
      dependencies.stopwatch.start();
      setState(() {});
    }
  }

  void timerStop() {
    if (dependencies.stopwatch.isRunning) {
      dependencies.stopwatch.stop();
      // maybe restart
      setState(() {});
    } else {
      //dependencies.stopwatch.start();
    }
  }
}

//-------------------------------------------------------------------
class TimerText extends StatefulWidget {
  TimerText({this.dependencies});
  final Dependencies dependencies;

  TimerTextState createState() =>
      new TimerTextState(dependencies: dependencies);
}

class TimerTextState extends State<TimerText> {
  TimerTextState({this.dependencies});
  final Dependencies dependencies;
  Timer timer;
  int milliseconds;

  @override
  void initState() {
    timer = new Timer.periodic(
        new Duration(milliseconds: dependencies.timerMillisecondsRefreshRate),
        callback);
    super.initState();
  }

  @override
  void dispose() {
    timer?.cancel();
    timer = null;
    super.dispose();
  }

  void callback(Timer timer) {
    if (milliseconds != dependencies.stopwatch.elapsedMilliseconds) {
      milliseconds = dependencies.stopwatch.elapsedMilliseconds;
      final int hundreds = (milliseconds / 10).truncate();
      final int seconds = (hundreds / 100).truncate();
      final int minutes = (seconds / 60).truncate();
      final ElapsedTime elapsedTime = new ElapsedTime(
        hundreds: hundreds,
        seconds: seconds,
        minutes: minutes,
      );
      for (final listener in dependencies.timerListeners) {
        listener(elapsedTime);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new RepaintBoundary(
          child: new SizedBox(
            height: 72.0,
            child: new MinutesAndSeconds(dependencies: dependencies),
          ),
        ),
        new RepaintBoundary(
          child: new SizedBox(
            height: 72.0,
            child: new Hundreds(dependencies: dependencies),
          ),
        ),
      ],
    );
  }
}

class MinutesAndSeconds extends StatefulWidget {
  MinutesAndSeconds({this.dependencies});
  final Dependencies dependencies;

  MinutesAndSecondsState createState() =>
      new MinutesAndSecondsState(dependencies: dependencies);
}

class MinutesAndSecondsState extends State<MinutesAndSeconds> {
  MinutesAndSecondsState({this.dependencies});
  final Dependencies dependencies;

  int minutes = 0;
  int seconds = 0;

  @override
  void initState() {
    dependencies.timerListeners.add(onTick);
    super.initState();
  }

  void onTick(ElapsedTime elapsed) {
    if (elapsed.minutes != minutes || elapsed.seconds != seconds) {
      setState(() {
        minutes = elapsed.minutes;
        seconds = elapsed.seconds;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');
    return new Text('$minutesStr:$secondsStr', style: dependencies.textStyle);
  }
}

class Hundreds extends StatefulWidget {
  Hundreds({this.dependencies});
  final Dependencies dependencies;

  HundredsState createState() => new HundredsState(dependencies: dependencies);
}

class HundredsState extends State<Hundreds> {
  HundredsState({this.dependencies});
  final Dependencies dependencies;

  int hundreds = 0;
  bool tick = false;

  @override
  void initState() {
    dependencies.timerListeners.add(onTick);
    super.initState();
  }

  void onTick(ElapsedTime elapsed) {
    if (elapsed.hundreds != hundreds) {
      setState(() {
        hundreds = elapsed.hundreds;
        tick = !tick;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    //print(hundreds);
    //print(hundreds % 100);
    //String hundredsStr = (hundreds % 100).toString().padLeft(2, '0');
    String ss = tick ? " " : ".";
    return new Text(ss, style: dependencies.textStyle);
  }
}
