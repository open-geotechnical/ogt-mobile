


import 'package:intl/intl.dart';

var humanReadableDateFormat = new DateFormat('dd MMM yy');
String date_format(DateTime d){
  if(d == null) {
    return "";
  }
  return humanReadableDateFormat.format(d);
}

DateTime to_date(dynamic s){
  if(s == null){
    return null;
  }
  return DateTime.parse(s);
}

var dbDateFormatter = new DateFormat('yyyy-MM-dd');
String date2js(DateTime d){
  if(d == null) {
    return "";
  }
  return dbDateFormatter.format(d);
}

var tsDateFormatter = new DateFormat('yyyy-MM-dd Hms');
String date_file_ts(){
  return  tsDateFormatter.format(DateTime.now());
}
