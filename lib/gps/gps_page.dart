

import 'dart:async';


import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong/latlong.dart' as latlon;

import '../drawer.dart' as drawer;

var geolocator = Geolocator();
var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);






class GpsPage extends StatelessWidget {


  var txtLatWsg = new TextEditingController();
  var txtLonWsg = new TextEditingController();

  var txtLatDM = new TextEditingController();
  var txtLonDM = new TextEditingController();


  StreamSubscription<Position> positionStream;

  @override
  Widget build(BuildContext context) {

    this.positionStream = geolocator.getPositionStream(locationOptions).listen(
        (Position position) {
        if(position == null){
          this.clear();
          return;
        }
        this.showPosition(position);
      });
    this.positionStream.pause();

    return Scaffold(
        appBar: AppBar(
          title: Text("GPS Position"),
        ),

        drawer: drawer.AppDrawer(),

        body: Column(
          children: <Widget>[
            Row (
              crossAxisAlignment: CrossAxisAlignment.start,
              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text('Lat'),
                Text('Lon'),
              ],
            ),
            Row (
              //childred: EdgeInsets.all(5),
              crossAxisAlignment: CrossAxisAlignment.start,
              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(child: TextField(controller: this.txtLatWsg)),
                Expanded(child: TextField(controller: this.txtLonWsg))
              ],
            ),
            Row (
              //childred: EdgeInsets.all(5),
              crossAxisAlignment: CrossAxisAlignment.start,
              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(child: TextField(controller: this.txtLatDM)),
                Expanded(child: TextField(controller: this.txtLonDM))
              ],
            ),
            Row (
              crossAxisAlignment: CrossAxisAlignment.start,
              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text(''),
                RaisedButton(onPressed: this.getPostion, child: Text("Refresh"))
              ],
            ),

            Row (
              crossAxisAlignment: CrossAxisAlignment.start,
              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text('TODO: integrate with ags and os.northings.eastings etc'),

              ],
            )

          ]
        ),

    );
  }


  void getPostion() async {
    Position position = await geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high);
    this.showPosition(position);
  }

  void showPosition(Position position){
    this.txtLatWsg.text = position.latitude.toString();
    this.txtLonWsg.text = position.longitude.toString();

    this.txtLatDM.text = latlon.decimal2sexagesimal(position.latitude);
    this.txtLonDM.text = latlon.decimal2sexagesimal(position.longitude);
  }

  void clear(){
    this.txtLatWsg.text = "?";
    this.txtLonWsg.text = "?";

    this.txtLatDM.text = "?";
    this.txtLonDM.text = "?";
  }


}
