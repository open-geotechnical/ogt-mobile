
import "dart:convert";
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
//import 'package:flutter/services.dart' show rootBundle;


import 'ags4/ags4_models.dart' as Ags4;

const WWW_SITE = "openg";
const PROJECT_SITE = "https://gitlab.com/open-geotechnical/ogt-mobile";

//bool dev_mode = true;
bool devMode = true;

Ags4.Ags4DataDict ags4DD = new Ags4.Ags4DataDict();

void init(){
  ags4DD.loadFromAssetsFile();
}

String toJSON(data){
  var encoder = new JsonEncoder.withIndent("  ");
  return encoder.convert(data);
}
Map<String, dynamic> fromJSON(strr){
  return jsonDecode(strr);
}

//============================================================
LocalStorage localStorage = new LocalStorage();

// simple proxy to local filesystem atmo
class LocalStorage {

  bool debug = false;
  LocalStorage();

  // Gets local Dir ?
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> _localFile(String f_name) async {
    final path = await _localPath;
    return File('$path/${f_name}');
  }

  Future<String> readFile(String f_name) async {
    try {
      final file = await _localFile(f_name);
      return await file.readAsString();

    } catch (e) {
      print("err.readfile=" + f_name + "----" + e.toString());
      return null;
    }
  }


  Future<File> writeFile(String f_name, String json_str) async {
    final file = await _localFile(f_name);
    return file.writeAsString('$json_str');
  }

  Future<List<String>> listDir(String prefixFilter) async {

    List<String> lst = List<String>();
    var fp = await _localPath;
    var dir = new Directory(fp);
    //dir.create();

    List contents = dir.listSync();
    for (var fileOrDir in contents) {
      //print(fileOrDir.path);

      if (fileOrDir is File) {
        var fname = basename(fileOrDir.path);
        if(prefixFilter != null){
          if(fname.startsWith(prefixFilter)){
            lst.add(fname);
          }
        } else {
          lst.add(fname);
        }

      } else if (fileOrDir is Directory) {
        //print(fileOrDir.path);
      }
    }
    return lst;
  }


}

