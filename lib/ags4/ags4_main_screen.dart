import 'package:flutter/material.dart';

import '../drawer.dart';
import '../G.dart' as G;

import "ags4_models.dart";
import "ags4_group_screen.dart";
import "ags4_groups_listview.dart";

class Ags4DataDictScreen extends StatefulWidget {
  Ags4DataDictScreen();

  factory Ags4DataDictScreen.forDesignTime() {
    // TODO: add arguments
    return new Ags4DataDictScreen();
  }

  @override
  Ags4DataDictScreenState createState() => Ags4DataDictScreenState();
}

class Ags4DataDictScreenState extends State<Ags4DataDictScreen> {

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(primarySwatch: Colors.green),
        child: DefaultTabController(
            length: 4,
            initialIndex: 0,
            child: new Scaffold(
              drawer: AppDrawer(),

              appBar: AppBar(
                title: Text("AGS4 Ref"),
                bottom: TabBar(
                  tabs: [
                    Tab(text: "Groups"),
                    Tab(text: "Types"),
                    Tab(text: "Units"),
                    Tab(text: "About")
                  ],
                ),
                actions: <Widget>[
                  IconButton(icon: Icon(Icons.refresh),
                    onPressed: () { G.ags4DD.loadFromAssetsFile(); },
                  )
                ],
              ),
              body: TabBarView(
                children: [
                Ags4GroupsListView(onItemTap: this.onItemTap),
                  this._buildDataTypesWidget(),
                  this._buildUnitsWidget(),
                  this._buildInfoWidget()
                ],
              ),
            )));
  }

  void onItemTap(Group grp){
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) =>
          Ags4GroupScreen(group: grp),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  Widget _buildUnitsWidget() {
    var abbrs = G.ags4DD.units();
    if (abbrs == null) {
      return Text("Oops");
    }
    return _buildAbbrsList(abbrs);
  }

  Widget _buildDataTypesWidget() {
    var abbrs = G.ags4DD.dataTypes();
    if (abbrs == null) {
      return Text("Oops");
    }
    return _buildAbbrsList(abbrs);
  }

  Widget _buildAbbrsList(List<Abbr> lst){
    return ListView.builder(
      itemCount: lst.length,
      padding: const EdgeInsets.all(5.0),
      itemBuilder: (BuildContext context, int idx) {

        return ListTile(
          isThreeLine: false,
          dense: true,
          contentPadding: EdgeInsets.all(5),
          title: Text('${lst[idx].code}',
            style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 17.0,
            )),
          subtitle: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '${lst[idx].description}',
                style: TextStyle(
                  fontWeight: FontWeight.normal, fontSize: 15.0, color: Colors.black
                )),
            ],
          ),
          trailing: Column(children: <Widget>[
            //Padding(padding: EdgeInsets.all(5.0)),
            CircleAvatar(
              backgroundColor: Colors.grey,
              radius: 20.0,
              child: Text(
                '${lst[idx].list}',
                style: TextStyle(
                  fontSize: 12.0,
                  color: Colors.white,
                ),
              ),
            ),
          ]),
          onTap: () {
            //this.widget.onItemTap(groupsFiltered[idx]);
          },
        );
      });
  }

  Widget _buildInfoWidget() {
    return Text("AHS info widget");
  }
}
