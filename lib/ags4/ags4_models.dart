
import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;


/// Represents an Ags4 Group
class Group {

  String group_code;
  String group_description;
  String classification;
  bool group_required;
  String parent;
  List<String> children;

  List<Heading> headings;
  List<String> notes;


  Group.fromJson(Map<String, dynamic> jmap){

    // Groups details
    this.group_code = jmap['group_code'];
    this.group_description = jmap['group_description'];
    this.classification = jmap['class'];
    this.group_required = jmap['group_required'];

    this.parent = jmap['parent'];
    this.children = new List<String>();
    var kids = jmap['children'];
    if(kids != null){
      for(var i = 0; i < kids.length; i ++){
        this.children.add( kids[i] );
      }
    }


    // Headings list
    this.headings = new List<Heading>();
    List<dynamic> heads = jmap['headings'];
    for(var i = 0; i < heads.length; i ++){
      this.headings.add( Heading.fromJson(heads[i]));
    }

    // Notes list
    this.notes = new List<String>();
    var nts = jmap['notes'];
    if(nts != null) {
      for (var i = 0; i < nts.length; i ++) {
        this.notes.add(nts[i]);
      }
    }

  }
}

/// Represents an Ags4 Heading
class Heading {
  String group_code;
  String head_code;
  String head_description;
  String data_type;
  String unit;
  bool required;
  String example;

  Heading.fromJson(Map<String, dynamic> jmap){
    this.group_code = jmap['group_code'];
    this.head_code = jmap['head_code'];
    this.head_description = jmap['head_description'];
    this.data_type = jmap['data_type'];
    this.unit = jmap['unit'];
    this.required = jmap['required'];
    this.example = jmap['example'];
  }
}

/// Represents an Ags4 Abberviation
class Abbr {

  String code;
  String description;
  String list;

  Abbr.fromJson(Map<String, dynamic> jmap){
    this.code = jmap['code'];
    this.description = jmap['description'];
    this.list = jmap['list'];
  }
}



/// TODO: Dart newbie problems
/// Reads the assets/ags4.json file
/// and intention of make a class with
/// validators
class Ags4DataDict {

  List<Group> groups = new List<Group>();
  Map<String, List<Abbr>> abbrs = new Map<String, List<Abbr>>();

  final List<String> groups_leading = new List<String>();
  final List<String> groups_trailing = new List<String>();
  final List<String> groups_required_list = new List<String>();

  final List<String> classes = new List<String>();

  Ags4DD() {
    this.loadFromAssetsFile();
  }

  Future<String> _getAgs4ddFile() async {
    return await rootBundle.loadString('assets/ags4.json');
  }

  void loadFromAssetsFile(){
    this._getAgs4ddFile().then((str) {

      Map<String, dynamic> data = json.decode(str);

      this.abbrs.clear();
      this.groups.clear();
      this.groups_leading.clear();
      this.groups_trailing.clear();
      this.groups_required_list.clear();

      // abbreviations and groups
      this.loadAbbrs( data['abbrs'] );
      this.loadGroups( data['groups'] );

      // groups sort
      var items = data['groups_sort'];
      for(var i = 0; i < items['leading'].length; i++){
        groups_leading.add(items['leading'][i]);
      }
      for(var i = 0; i < items['trailing'].length; i++){
        groups_trailing.add(items['trailing'][i]);
      }

    }).catchError((e){
      // TODO
      print(e);
    });


  }

  List<Group> get groups_required {
    return this.groups_required_list.map((gcode) => this.getGroup(gcode)).toList();
  }

  void loadGroups(Map<String, dynamic> data){
    this.classes.clear();
    var grp_codes = data.keys.toList()..sort();
    for(var i = 0; i < grp_codes.length; i++){
      var gg = Group.fromJson( data[grp_codes[i]] );
      this.groups.add(  gg );
      if(!this.classes.contains(gg.classification)){
        this.classes.add(gg.classification);
      }
      if(gg.group_required == true){
        this.groups_required_list.add(gg.group_code);
      }
    }
    this.classes.sort();
    this.classes.insert(0, "-- Filter Class --");
  }

  void loadAbbrs(Map<String, dynamic> data){

    var head_codes = data.keys.toList()..sort();
    for(var i = 0; i < head_codes.length; i++){
      var hc = head_codes[i];
      this.abbrs[hc] = new List<Abbr>();
      if(data[hc] == null ){
        continue;
      }
      List<dynamic> rows = data[hc];
      rows.forEach((v) {
        this.abbrs[hc].add( Abbr.fromJson( v ));
      });

    }
  }

  List<Abbr> abbrsFromHeadCode(String head_code) {
    var lst = this.abbrs[head_code];
    if(lst == null){
      return null;
    }
    return lst;
  }

  List<Abbr> dataTypes(){
    print("---------------dataTypes");
    var lst = this.abbrs["DICT_DTYP"];
    if(lst == null){
      return null;
    }
    lst.sort((Abbr a, Abbr b) => a.code.compareTo(b.code));
    return lst;
  }

  List<Abbr> units(){
    print("---------------units");
    var lst = this.abbrs["UNIT_UNIT"];
    if(lst == null){
      return null;
    }
    lst.sort((Abbr a, Abbr b) => a.code.compareTo(b.code));
    return lst;

  }

//  List<Group> groupsRequired(){
//    return this.groups.where((grp) => grp.group_required == true).toList();
//  }

  Group getGroup(String grp_code){
    return this.groups.firstWhere((grp) => grp.group_code == grp_code, orElse: () => null);
  }

  // return a list list of group in prefered sort order
  List<String> sortGroups(List<String> origLst){

    // clone of original to remove items from
    List<String> xlist = []..addAll(origLst);

    // return list
    var sorted = List<String>();

    // add leading groups
    this.groups_leading.forEach((grp) {
      if(xlist.contains(grp)){
        sorted.add(grp);
        xlist.remove(grp);
      }
    });

    // make temp with trailing list
    var trails = List<String>();
    this.groups_trailing.forEach((grp) {
      if(xlist.contains(grp)){
        trails.add(grp);
        xlist.remove(grp);
      }
    });

    // sort whats left and add
    xlist.sort();
    sorted.addAll(xlist);

    // finally add trailing
    sorted.addAll(trails);

    return sorted;
  }




}




