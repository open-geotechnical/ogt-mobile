import 'package:flutter/material.dart';

import '../G.dart' as G;
import '../style.dart' as S;

import 'ags4_models.dart';
import "ags4_widgets.dart";

class Ags4GroupScreen extends StatefulWidget {
  Group group;

  Ags4GroupScreen({Key key, Group this.group}) : super(key: key);

  factory Ags4GroupScreen.forDesignTime() {
    // TODO: add arguments
    return new Ags4GroupScreen();
  }

  @override
  Ags4GroupScreenState createState() => Ags4GroupScreenState();
}

class Ags4GroupScreenState extends State<Ags4GroupScreen>
    with TickerProviderStateMixin {
  TabController _tabController;
  int _selTabIdx = 0;

  Color _rowColor = Colors.grey;

  @override
  void initState() {
    super.initState();

    _tabController = new TabController(
        vsync: this, length: 2, initialIndex: this._selTabIdx);
    _tabController.addListener(() {
      setState(() {
        this._selTabIdx = _tabController.index;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(primarySwatch: S.ags4),
        child: new Scaffold(
            appBar: AppBar(
              title: Text("AGS4 Group: ${this.widget.group.group_code}"),
              //backgroundColor: S.main,
            ),
            body:
                // header
                Container(
                    //padding: EdgeInsets.all(4),
                    color: S.ags4,

                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Flexible(
                            child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                            "${widget.group.group_description}",
                            style: TextStyle(color: Colors.white, fontSize: 18.0),
                          ),
                        )),
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text("${widget.group.classification}",
                              style: TextStyle(color: Colors.white, fontSize: 13.0),

                            ),
                        )),
                        Row(children: <Widget>[
                          Expanded( child: GroupChips(title: "Parent", groups: [])),
                          Expanded( child: GroupChips(
                            title: "Children",
                            groups: this.widget.group.children,
                            onSelect: this.onOpenGroup)),
                        ],)
                      ],
                    ))));
  }
  void onOpenGroup(String group_code){
    print("on_open" + group_code);
  }



  Widget _buildBody() {
    return Column(children: <Widget>[
      // group details
      Container(
        //padding: EdgeInsets.all(4),
        color: S.ags4,
        child: _buildHeader(),
      ),

      Container(
        color: S.ags4,
        child: TabBar(
          indicatorColor:
              Colors.yellow, //this._selTabIdx == 0 ? C.coc : C.sample,
          controller: this._tabController,
          tabs: [
            Tab(text: "Headings"),
            Tab(text: "Notes - " + widget.group.notes.length.toString()),
          ],
        ),
      ),

      Flexible(
        child: TabBarView(
          controller: this._tabController,
          children: [
            //this._buildHeadingsLV(),
            //this._buildNotesWidget()
            //this._buildSamplesListView(),
            Text("One"), Text("Tow")
            //SamplesTable(samples: this.samples,)
            //this._buildStoresForm()
          ],
        ),
      )
    ]);
  }

  Column _buildHeader() {
    return Column(
      // mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Flexible(
            child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text(
            "${widget.group.group_description}",
            style: TextStyle(color: Colors.black),
          ),
        )),
        Flexible(
            child: Text("${widget.group.classification}",
                style: TextStyle(color: Colors.black))),
      ],
    );
  }

  Widget _buildHeadingsLV() {
    return ListView.builder(
        itemCount: this.widget.group.headings.length,
        padding: const EdgeInsets.all(5.0),
        itemBuilder: (BuildContext context, int idx) {
          Heading rec = this.widget.group.headings[idx];
          var hgrp = rec.head_code.split("_")[0];
          if (widget.group.group_code == hgrp) {
            _rowColor = Colors.green;
          } else {
            _rowColor = Colors.grey.shade300;
          }
          return Row(
            children: <Widget>[
              Container(
                // color line down side..
                color: _rowColor,
                width: 2,
              ),
              Text('${rec.head_code}',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))
            ],
          );
          return ListTile(
            contentPadding: EdgeInsets.all(3),
            leading: Container(
              // color line down side..
              color: _rowColor,
              width: 2,
            ),
            title: Row(children: <Widget>[
              Expanded(
                  // Head code
                  child: Text('${rec.head_code}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ))),

              // Head descritptions
              Text(rec.unit == null ? "" : '${rec.unit}',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                  ))
            ]),
            subtitle: Text('${rec.head_description}',
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                )),
            trailing: CircleAvatar(
              backgroundColor:
                  rec.data_type == "PA" ? Colors.lightGreen : Colors.white,
              radius: 20.0,
              child: Text(
                '${this.widget.group.headings[idx].data_type}',
                style: TextStyle(
                  fontSize: 15.0,
                  color: Colors.black,
                ),
              ),
            ),
            onTap: rec.data_type == "PA"
                ? () {
                    print("none");
                  }
                : null,
          );
        });
  }

  Widget _buildNotesWidget() {
    if (this.widget.group.notes.length == 0) {
      return Text("None");
    }
    return ListView.builder(
        itemCount: this.widget.group.notes.length,
        padding: const EdgeInsets.all(5.0),
        itemBuilder: (BuildContext context, int idx) {
          return ListTile(
              contentPadding: EdgeInsets.all(3),
              title: Text(widget.group.notes[idx]));
        });
  }
}





