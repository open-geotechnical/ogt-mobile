
import 'package:flutter/material.dart';

//======================================================================================================================
class GroupChips extends StatelessWidget {

  String title;
  List<String> groups;
  final Function onSelect;


  GroupChips({
    String this.title,
    List<String> this.groups,
    Function this.onSelect,
  });

  //======
  @override
  Widget build(BuildContext context) {

    List<Widget> kids;

    if(groups.length == 0) {

      kids = [
        Container(
          padding: EdgeInsets.all(15),

          child: Text("-")
        )
      ];
    } else {
      kids = groups.map<Widget>((String group_code) {
        return ActionChip(
          padding: EdgeInsets.all(2.0),
          key: ValueKey<String>(group_code),
          //backgroundColor: C.stores,
          onPressed: this.onSelect,
          label: Text(group_code, style: TextStyle(color: Colors.black)),
        );
      }).toList();
    }

    return Column(
      children: <Widget>[
        Text(this.title),
        InkWell(
          //onTap: this.onTap,
          child: Wrap(
            spacing: 4.0, // gap between adjacent chips
            runSpacing: 2.0,
            children: kids,
          )
        ),
      ],
    );
  }


}
