import 'package:flutter/material.dart';

//import '../drawer.dart';
import '../G.dart' as G;
import '../style.dart' as S;

import "ags4_models.dart";

class Ags4GroupsListView extends StatefulWidget {

  ValueChanged<Group> onItemTap;
  Ags4GroupsListView({Key key, this.onItemTap}) : super(key: key);

  @override
  Ags4GroupsListViewState createState() => new Ags4GroupsListViewState();
}

class Ags4GroupsListViewState extends State<Ags4GroupsListView> with TickerProviderStateMixin {

  List<Group> groupsFiltered = new List<Group>();
  final TextEditingController _filterTextC = new TextEditingController();
  String _filterText = "";

  @override
  void initState(){
    super.initState();

    _filterTextC.addListener(() {
//      if (_filterTextC.text.isEmpty) {
//        setState(() {
//          _filterText = "";
//        });
//      } else {
//
//      }
      updateFilter();
    });
  }

  void updateFilter(){
    setState(() {
      _filterText = _filterTextC.text.toLowerCase().trim();
      groupsFiltered.clear();
      if ( !_filterText.isEmpty ) {

        for (int i = 0; i < G.ags4DD.groups.length; i++) {

          if (G.ags4DD.groups[i].group_description.toLowerCase().contains(_filterText.toLowerCase())) {
            groupsFiltered.add(G.ags4DD.groups[i]);
          }
        }
      } else {
        for (int i = 0; i < G.ags4DD.groups.length; i++) {
          groupsFiltered.add(G.ags4DD.groups[i]);
        }
      }
    });
  }

  @override
  void dispose(){
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[

//        DropdownButton<String>(
//            isExpanded: true,
//            value: this._filterClass,
//            items: G.ags4DD.classes.map((String value) {
//            return new DropdownMenuItem<String>(
//              value:  value,
//              child: new Text(value),
//            );
//          }).toList(),
//          onChanged: (nuVal) {
//            setState(() {
//              this._filterClass = nuVal;
//              print(this._filterClass);
//              print(nuVal);
//            });
//          },
//        ),
        // Filter text
        TextField(
          controller: this._filterTextC,
          autofocus: true,
          autocorrect: false,
          textInputAction: TextInputAction.none,
          keyboardType: TextInputType.text,
          decoration: new InputDecoration(
            prefixIcon: new Icon(Icons.search),
            suffixIcon: IconButton(
              icon: Icon(Icons.clear),
              onPressed: () {
                this._filterTextC.text = "";
              }
            ),
            hintText: 'filter...'
          ),
        ),

        // Listview
        Expanded(
          child: _buildListView(),
        ),
      ],
    );
  }

  ListView _buildListView() {

//    groupsFiltered.clear();
//    if ( !_filterText.isEmpty ) {
//
//      for (int i = 0; i < G.ags4DD.groups.length; i++) {
//
//        if (G.ags4DD.groups[i].group_description.toLowerCase().contains(_filterText.toLowerCase())) {
//          groupsFiltered.add(G.ags4DD.groups[i]);
//        }
//      }
//    } else {
//      for (int i = 0; i < G.ags4DD.groups.length; i++) {
//        groupsFiltered.add(G.ags4DD.groups[i]);
//      }
//    }

    return ListView.builder(
          itemCount: groupsFiltered.length,
          padding: const EdgeInsets.all(5.0),
          itemBuilder: (BuildContext context, int idx) {

            return ListTile(
              isThreeLine: true,
              contentPadding: EdgeInsets.all(5),
              title: Text('${groupsFiltered[idx].group_code}',
                style: TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 16.0,
                  color: groupsFiltered[idx].group_required == true ? S.groupReq : Colors.grey
                )),
              subtitle: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '${groupsFiltered[idx].group_description}',
                    style: TextStyle(
                      fontWeight: FontWeight.normal, fontSize: 18.0, color: Colors.black
                    )),
                  Text(
                    '${groupsFiltered[idx].classification}',
                    style: TextStyle(
                      fontWeight: FontWeight.normal,  fontSize: 14.0
                    )),
                ],
              ),
              trailing: Column(children: <Widget>[
                //Padding(padding: EdgeInsets.all(5.0)),
                CircleAvatar(
                  backgroundColor: groupsFiltered[idx].group_required == true ? S.groupReq : S.groupNotReq,
                  radius: 20.0,
                  child: Text(
                    '${groupsFiltered[idx].headings.length}',
                    style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ]),
              onTap: () {
                this.widget.onItemTap(groupsFiltered[idx]);
              },
            );
          });
  }

}
