import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

import '../drawer.dart' as drawer;

const TOKEN =
    "pk.eyJ1IjoicGVkcm9tb3JnYW4iLCJhIjoiY2pwZnBhNWVqMGM4dzNwbGd4eTM5bjlrYyJ9.kHIpi9wvU7MLNusHc_T_Zg";

class MapPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Map [under dev]"),
        ),
        drawer: drawer.AppDrawer(),
        body: FlutterMap(
          options: new MapOptions(
            center: LatLng(51.5, - 0.09),
            zoom: 13.0,
          ),
          layers: [
            new TileLayerOptions(
              urlTemplate: "https://api.tiles.mapbox.com/v4/"
                  "{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}",
              additionalOptions: {
                'accessToken': TOKEN,
                'id': 'mapbox.streets',
              },
            ),
            new MarkerLayerOptions(
              markers: [
                new Marker(
                  width: 80.0,
                  height: 80.0,
                  point: new LatLng(51.5, -0.09),
                  builder: (ctx) => new Container(
                        child: new FlutterLogo(),
                      ),
                ),
              ],
            ),
          ],
        ));
  }
}
