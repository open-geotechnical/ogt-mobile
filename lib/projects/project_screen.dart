
import 'package:flutter/material.dart';

import '../data/data_models.dart';
import '../data/data_widgets.dart';
import '../data/form_screen.dart';
import '../G.dart' as G;
import '../style.dart' as S;


class ProjectScreen extends StatefulWidget {


  ProjectScreen({Key key}) : super(key: key);

  factory ProjectScreen.forDesignTime() {
    // TODO: add arguments
    return new ProjectScreen();
  }


  @override
  ProjectScreenState createState() => ProjectScreenState();

}

class ProjectScreenState extends State<ProjectScreen>
{
  bool _loading = true;
  Project project;

  final _formKeyProj = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {

    if(_loading){
      return CircularProgressIndicator();
    }

    return Theme(
      data: ThemeData(primarySwatch: S.project),
      child: DefaultTabController(
        length: 2,
        initialIndex: 0,

        child: new Scaffold(

          appBar: AppBar(
            title: Text("Project: "),
            bottom: TabBar(
              tabs: [
                Tab(text: "Groups"),
                Tab(text: "Details"),
                //Tab(text: "Units"),
                //Tab(text: "About")
              ],
            ),
          ),

          body: TabBarView(
            children: [
              GroupsListView(this.project),
              //this._buildProjForm()
              Text("here")
            ],
          ),


        )
      )
    );
  }

  @override
  void initState(){
    super.initState();

    this.loadData();
    this._loading = false;
  }

  void loadData(){
    this.project = Project.createDefault();
    this.project.addGroupFromCode("AELO");
    this.project.addGroupFromCode("SAMP");
    this.project.addGroupFromCode("ICBR");
    print("proj=");
    print(this.project);
  }


//  Widget _buildGroupsList(){
//
//    var grpCodes = G.ags4DD.sortGroups(this.project.groups_list);
//
//    return ListView.builder(
//      itemCount: grpCodes.length,
//      padding: const EdgeInsets.all(15.0),
//      itemBuilder: (context, index) {
//
//        Group grp = this.project.groups_map[grpCodes[index]];
//
//        return ListTile(
//          title: Text('${grp.group_description}',
//                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500)
//          ),
//          subtitle: Text('${grp.group_code}',
//            style: TextStyle(color: grp.group_required == true ? Colors.purple : Colors.green)
//          ),
//          trailing: CircleAvatar(
//            child: Text('${grp.data_rows.length}'),
//          ),
//          onTap: (){
//            this.edit_group(index, grp.group_code);
//            //widget.onSelected(storesFiltered[index]);
//          },
//        );
//      }
//    );
//  }

  void edit_group(int idx, String grp_code) async {
    print("edit_group" + grp_code);

    if(grp_code == "PROJ"){

      var rec = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => FormScreen(project: this.project, group_code: grp_code),
        ),
      );
      if(rec != null){
        setState((){

//          if(store_id == 0){
//            G.cache.stores.insert(0, store);
//          } else {
//            G.cache.stores[index] = store;
//          }
        });
      }

    }

  }

//  Widget _buildProjForm(){
//
//    var widgets = new List<Widget>();
//    Group grp = this.project.getGroup("PROJ");
//    for(var i = 0; i < grp.headings.length; i++){
//        var hd = grp.headings[i];
////        var widget = TextFormField(
////              decoration: InputDecoration(
////                labelText: hd.head_description,
////                helperText: hd.head_code
////              ),
////              validator: (value) {
////                if (value.isEmpty) {
////                  return 'Please enter some text';
////                }
////              },
////            );
//        var widget = makeFormField(hd);
//        widgets.add(widget);
//    }

//    return Form(
//      key: _formKeyProj,
//      child: ListView(
//        padding: EdgeInsets.all(5.0),
//        children: widgets,
//      )
//    );
//
//  }
}
