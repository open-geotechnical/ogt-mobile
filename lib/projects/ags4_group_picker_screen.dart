import 'package:flutter/material.dart';

import '../drawer.dart';
import '../G.dart' as G;
import '../style.dart' as S;

import "../data/data_models.dart";
import "../ags4/ags4_models.dart" as ags4;
//import "../ags4/ags4_models.dart";

import "../ags4/ags4_groups_listview.dart";

class Ags4GroupPickerScreen extends StatefulWidget {

  Project project;

  Ags4GroupPickerScreen(Project this.project);

  @override
  Ags4GroupPickerScreenState createState() => Ags4GroupPickerScreenState();
}

class Ags4GroupPickerScreenState extends State<Ags4GroupPickerScreen> {

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(primarySwatch: Colors.green),
      child:  new Scaffold(
          appBar: AppBar(
            title: Text("Pick AGS Groups"),
//            leading: IconButton(icon: Icon(Icons.clear),
//              onPressed: (){
//                Navigator.pop(context);
//              },
//            ),
//            actions: <Widget>[
//              RaisedButton.icon(
//                icon: Icon(Icons.save_alt, color: Colors.yellow,),
//                label: Text("Ok", style: TextStyle(color: Colors.yellow)),
//                color: Colors.green,
//                onPressed: (){
//
//                },
//              )
            //],
          ),
          body: Column(
            children: <Widget>[
              _buildChips(),
              Expanded(child: Ags4GroupsListView(onItemTap: this.onItemTap)),
            ],
          ),
      )
    );
  }

  void onItemTap(ags4.Group grp){
    //print(grp.group_code);
    setState(() {
      this.widget.project.addGroupFromCode(grp.group_code);
    });

  }

  Widget _buildChips(){

    List<Widget> kids;
    var grps = widget.project.groups_sorted();
    if(grps.length == 0) {

      kids = [
        Container(
          padding: EdgeInsets.all(15),

          child: Text("Tap to add group")
        )
      ];
    } else {
      kids = grps.map<Widget>((Group grp) {
        return Chip(
          padding: EdgeInsets.all(2.0),
          key: ValueKey<Group>(grp),
          backgroundColor: grp.group_required == true ?  S.groupReq :  S.groupNotReq,
          label: Text(grp.group_code, style: TextStyle(color: Colors.white)),
//          avatar: grp.group_required == true
//            ? null
//            : CircleAvatar(
//              maxRadius: 15,
//              backgroundColor: Colors.black,
//              foregroundColor: Colors.white,
//              child: Icon(Icons.clear)
//          ),
          onDeleted: grp.group_required == true ? null : (){
            print("dev");
            setState(() {
              this.widget.project.removeGroup(grp.group_code);
            });
          },
          deleteIconColor: Colors.white,
//          onDeleted: this.onDeleted == null ? null : () {
//            this.onDeleted(sto);
//            //setState(() {
//            //  this.stores.remove(sto);
//            //});
//          },
        );
      }).toList();
    }

    return InkWell(
      child: Wrap(
        spacing: 4.0, // gap between adjacent chips
        runSpacing: 2.0,
        children: kids,
      )
    );

  }

  @override
  void initState() {
    super.initState();
  }

}
