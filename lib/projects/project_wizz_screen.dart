

import 'package:flutter/material.dart';


import '../data/data_models.dart';
import '../data/data_widgets.dart';
import "package:ogt_mobile/projects/ags4_group_picker_screen.dart";
import '../G.dart' as G;
import '../style.dart' as S;



class ProjectWizardScreen extends StatefulWidget {
  @override
  ProjectWizardScreenState createState() => ProjectWizardScreenState();
}

class ProjectWizardScreenState extends State<ProjectWizardScreen> {

  int page = 0;
  Project project = null;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    var tits = ["Select template",
                "Add or remove groups",
                "Project details",
                "Create & Save"];
    Widget widget;
    Widget floatButton = null;

    switch (this.page) {
      case 0:
        widget = _buildSelectTemplate();
        break;

      case 1:
        widget = GroupsListView(this.project);
        floatButton = FloatingActionButton.extended(
            icon: Icon(Icons.add, ),
            label: Text("Add/Remove Groups"),
            backgroundColor: Colors.black87,
            onPressed: this.addGroup
        );
        break;

      case 2:
        widget = _buildForm();
        break;

      case 3:
        widget = _saveProj();
        break;

      default:
        widget = Text("ooops");
    }




    return Theme(
        data: ThemeData(primarySwatch: S.project),
        child: Scaffold(
            appBar: AppBar(
                title: Text("Create New Project"),
                leading: IconButton(icon: Icon(Icons.close),
                  onPressed: (){
                    Navigator.pop(context);
                  })
            ),

            floatingActionButton: floatButton,
            floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,

          body: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(child:
                        Text(tits[page], style: TextStyle(color: Colors.blue),)
                      ),

                      if(page > 0)
                        IconButton(
                          icon: Icon(Icons.arrow_back_ios),
                          onPressed: page == 0
                            ? null
                            : () {
                            setState(() {
                              page += -1;
                            });
                          },
                        ),
                      if(page > 0 && page < 3)
                        RaisedButton.icon(
                          color: S.actionBg,
                          label: Text("Next", style: TextStyle(color: S.actionFg,)),
                          icon: Icon(Icons.arrow_forward_ios, color: S.actionFg,),
                          onPressed: () {
                            setState(() {
                              page += 1;
                            });
                          },
                        ),

                    ],
                  ),
                  Expanded(child: widget)
                ],
              ),
            ))
        //this._buildProjForm()

        );
  }

  Widget _buildSelectTemplate() {
    var tpls = projectTemplates();
    print(tpls);

    return ListView.builder(
        padding: EdgeInsets.all(10.0),
        itemCount: tpls.length,
        itemBuilder: (context, index) {
          return ListTile(
            contentPadding: EdgeInsets.all(5),
            onTap: () {

              setState(() {
                this.project = Project.createFromTemplate(tpls[index]);
                print(tpls[index].title);
                print( this.project.toString() );
                page += 1;
              });


            },
            leading: Icon(Icons.arrow_forward_ios),
            title: Text(tpls[index].title),
          );
        });
  }

  Widget _buildForm(){
    Group grp = this.project.getGroup("PROJ");
    if(grp.data_rows.length == 0){
      grp.createDataRow();
    }
    return FormWidget(group: grp, dataRowIndex: 0);
  }

  void addGroup() async {
    print("ere");

    var grp = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Ags4GroupPickerScreen(this.project),
      ),
    );
    print("YES==" + grp);
  }

  Widget _saveProj(){

    return Column(
      children: <Widget>[
        Text("Filename"),
        RaisedButton.icon(
          color: S.actionBg,
          icon: const Icon(Icons.save, color: S.actionFg,),
          label: const Text("Create Project", style: const TextStyle( color: S.actionFg,)),
          onPressed: (){
            var err = this.project.save();
            print(err);
            if(err == null){
              Navigator.pop(context, this.project);
            }
          },

        )
      ]
    );

  }

}
