import 'package:flutter/material.dart';

import '../data/data_models.dart';
import '../drawer.dart';
import '../G.dart' as G;
import '../style.dart' as S;

//import "ags4_group_screen.dart";

class ProjectsScreen extends StatefulWidget {
  @override
  ProjectsScreenState createState() => ProjectsScreenState();
}

class ProjectsScreenState extends State<ProjectsScreen> {



  @override
  void initState() {
    super.initState();

    //_loadProjects();
  }

  void _loadProjects() async {
   var lst = await listProjects();
   print("u>>>>>>>>>>>>>es");
   print(lst);

  }



  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(primarySwatch: S.project),
        child: DefaultTabController(
            length: 2,
            initialIndex: 0,
            child: Scaffold(
              drawer: AppDrawer(),
              appBar: AppBar(
                title: Text("Projects"),
                actions: <Widget>[
                  IconButton(
                    icon: Icon(Icons.refresh),
                    tooltip: 'Refresh',
                    onPressed: this._loadProjects
                  ),
                  IconButton(
                    icon: Icon(Icons.add),
                    tooltip: 'New Project',
                    onPressed: this._newProject
                  ),
                ],
                bottom: TabBar(
                  tabs: [
                    Tab(text: "Active"),
                    Tab(text: "Archived"),

                    //Tab(text: "Units"),
                    //Tab(text: "About")
                  ],
                ),
              ),
              body: TabBarView(
                children: [
                  Text("here"),
                  //this._buildProjForm()
                  Text("here"),

                ],
              ),
            )));
  }



  void _newProject(){
     Navigator.pushNamed(context, '/project_wizz');
  }




}
