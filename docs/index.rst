.. _ogt-mobile:

Open GeoTechincal Mobile App
======================================

- This is OpenSource and using Google's [Flutter](https://flutter.io) platform
- This app is being actively developed and is WIP on alpha-1 on Android (IOS soon)

.. raw:: html
   :file: play_store.html


.. toctree::
    :maxdepth: 2

    screen_shots.md
    ideas_goals.md
    developer.md

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
