# Screenshots

A few shots from the developers.

### AGS Ref

<img width="300" src="_static/screen_shots/ags_ref.png">
<img width="300" src="_static/screen_shots/ags_group.png">

### Project wizard
<img width="300" src="_static/screen_shots/wizz_1.png">
<img width="300" src="_static/screen_shots/wizz_2.png">
<img width="300" src="_static/screen_shots/wizz_3.png">
<img width="300" src="_static/screen_shots/wizz_4.png">

### Development Stuff

<img width="300" src="_static/screen_shots/gps.png">
<img width="300" src="_static/screen_shots/map.png">
<img width="300" src="_static/screen_shots/stopwatch.png">


